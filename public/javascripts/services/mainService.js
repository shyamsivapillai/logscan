angular.module('mainService',[])

.factory('Main',function($http){
	var mainFactory = {};

	mainFactory.createProduct = function(productData){
		return $http.post('/api/createProduct',productData);
	}
    
	mainFactory.sellItem = function(scanCode){
		return $http.post('/api/sellItem',scanCode);
	}
    
	mainFactory.sellTwoItem = function(scanCode){
		return $http.post('/api/sellTwoItem',scanCode);
	}
    
    mainFactory.moveSecond = function(scanCode){
		return $http.post('/api/moveSecond',{scanCode:scanCode});
	}
    
    mainFactory.currentStock = function(scanCode){
		return $http.post('/api/createProduct',{scanCode:scanCode});
	}
    
    mainFactory.checkItem = function(scanCode){
        return $http.post('/api/checkItem',scanCode)
    }

	mainFactory.all = function(){
		return $http.get('/api/allProducts');
	}

	return mainFactory;
});
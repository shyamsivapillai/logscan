angular.module('mainCtrl', ['ngMaterial'])
.controller('mainController',function($mdDialog,$mdMedia,$mdToast,Main){

	var vm = this;

    vm.hello = "Hello world!!!";
    
    vm.productListData = [];
    vm.returnStockOne = "";
    vm.returnStockTwo = "";
    
    
    Main.all()
        .then(function(data){
        
        if(data.data.success){
            console.log("Successfully retrieved list of products");
            vm.productListData = data.data.products;
        }else{
            console.log("Error occurred in retrieving list of products");
            console.log(data.data);
            showToast(data.data.msg);
        }
        
        
    })

        function problemSpot(msg){
            if(!productListData){
              console.log("The problem occurs here in " + msg);
              showToast("The problem occurs here in " + msg);  
            }
            
        }

    vm.createProduct = function(productData){
        Main.createProduct(productData)
            .then(function(data){
            
            if(data.data.success){
                
                showToast("Successfully created product");
                
                
                if(data.data.new){
                    problemSpot("createProduct");
                    vm.productListData.push(data.data.product);
                }else{
                    problemSpot("createProduct-2");
                    for(i=0;i<productListData.length;i++){
                        if(data.data.product.itCode == vm.productListData[i].itCode && data.data.product.sizeIn == vm.productListData[i].sizeIn && data.data.product.model == vm.productListData[i].model){
                            vm.productListData[i] = data.data.product;
                            break;

                        }
                    }
                }
                
                
                
                vm.productData = {};
            }else{
                console.log("Error in creating product" + data.data.err + data.data.msg);
                showToast("Error in creating product");
                showToast(data.data.msg);
            }
            
        })
    }
    
    vm.moveProduct = function(moveData){
        
        Main.moveSecond(moveData)
            .then(function(data){
            if(data.data.success){
                console.log("Successfully moved data");
                
                var holder = serialSplitter(vm.moveData.scanCode);
                showToast(data.data.msg);
                problemSpot("createProduct");
                for(i=0;i<productListData.length;i++){
                    if(holder.itCode == vm.productListData[i].itCode && holder.sizeIn == vm.productListData[i].sizeIn && holder.model == vm.productListData[i].model){
                        
                        for(j=0;j<productListData[i].sizeArr.length;j++){
                            if(productListData[i].sizeArr[j].serial == holder.serial){
                                productListData[i].sizeArr[j].location = 2;
                                console.log("Successfully updated the table value");
                            }
                        }
                        
                        
                        
                        
                    }
                }
                
            }else{
                
                console.log("Error occurred in moving data");
                showToast(data.data.msg);
                
            }
        })
        
        
        
        
    }
    
    
    vm.sellProduct = function(sellData){
        Main.sellItem(sellData)
            .then(function(data){
            if(data.data.success){
                console.log("Successfully sold the data");
                showToast("Successfully sold data");
                vm.sellData = {};
            }else{
                console.log("Error in selling product" + data.data.err + data.data.msg);
                showToast("Product already sold");
            }
        })
    }
    
    vm.sellTwoProduct = function(sellData){
        Main.sellItem(sellData)
            .then(function(data){
            
            if(data.data.success){
                console.log("Successfully sold the data");
                showToast("Successfully sold data");
                vm.sellTwoData = {};
            }else{
                
                console.log("Error in selling product" + data.data.err + data.data.msg);
                showToast("Product already sold");
                
                
            }
            
            
            
            
            
        })
    }
    
    vm.checkProduct = function(checkData){
        vm.checkCarton = true;
        Main.checkItem(checkData)
            .then(function(data){
            if(data.data.success){
                console.log("Successfully checked the data");
                showToast("Successfully checked the data");
                vm.returnStockOne = data.data.returnStockOne;
                vm.returnStockTwo = data.data.returnStockTwo;
                vm.checkCarton = false;
                function clearStockDisplay(){
                    vm.returnStockOne = "";
                    vm.returnStockTwo = "";
                }
                
                setTimeout(clearStockDisplay,4000);
                vm.checkData = {};
            }
        })
    }
    
    
    // TOAST FUNCTION
    function showToast(message){
                $mdToast.show(
                    $mdToast.simple()
                        .content(message)
                        .position('top, right')
                        .hideDelay(3000)
                    );
            }
    
    
    //Warehouse 1 stock calculator
    vm.WstockCalc =function(inProduct){
        var k = 0;
        for(i=0;i<inProduct.sizeArr.length;i++){
            if(inProduct.sizeArr[i].location == 1 && inProduct.sizeArr[i].noShoes == inProduct.noi){
                k+=inProduct.sizeArr[i].noShoes;
            }
            
            
            if(inProduct.sizeArr[i].location == 1 && inProduct.sizeArr[i].noShoes < inProduct.noi){
                k+= inProduct.sizeArr[i].noShoes;
            }
        }
        
        
        console.log("Value of warehouse 1 is " + k);
        return k;
    }
    
    //Warehouse 2 stock calculator
    
    vm.WstockCalcTwo=function(inProduct){
        var k = 0;
        for(i=0;i<inProduct.sizeArr.length;i++){
            if(inProduct.sizeArr[i].location == 2 && inProduct.sizeArr[i].noShoes == inProduct.noi){
                k+=inProduct.sizeArr[i].noShoes;
            }
            
            if(inProduct.sizeArr[i].location == 2 && inProduct.sizeArr[i].noShoes < inProduct.noi){
                k+= inProduct.sizeArr[i].noShoes;
            }
        }
        return k;
    }
    
    vm.itemCount=function(inProduct){
        var k = 0;
        for(i=0;i<inProduct.sizeArr.length;i++){
            if(inProduct.sizeArr[i].location == 3){
                k+=inProduct.sizeArr[i].noShoes;
            }
            
            if(inProduct.sizeArr[i].noShoes < inProduct.noi && inProduct.sizeArr[i].location !== 3){
                k+=inProduct.noi - inProduct.sizeArr[i].noShoes;
            }
        }
        return k;
    }
    
    
    
    
    function serialSplitter(inSerial){
    
    var holder = inSerial.toString();
    // 10010139071700001. the first digit 1 stands for shoes. 001 stands for brand say adidas the next 01 stands for model 39 is the size 07 17 is month and year 00001 is serial number.
    
    //item code
    var itCode = parseInt(holder[0]);
    //brand
    var brand = parseInt(holder[1]+holder[2]+holder[3]);
    //model
    var model = parseInt(holder[4] + holder[5]);
    //size
    var sizeIn = parseInt(holder[6] + holder[7]);
    //date
    var dateIn = parseInt(holder[9] + holder[10] + holder[11] + holder[12]);
    //serial number
    var serial = parseInt(holder[13] + holder[14] + holder[15] + holder[16] + holder[17]);
    var inData = {
                 itCode:itCode,
                 brand:brand,
                 model:model,
                 sizeIn:sizeIn,
                 dateIn:dateIn,
                 serial:serial
                 };
    return inData;    
    
}
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
});
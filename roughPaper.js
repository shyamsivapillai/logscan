function serialSplitter(inSerial){
    
    var holder = inSerial.toString();
    // 10010139071700001. the first digit 1 stands for shoes. 001 stands for brand say adidas the next 01 stands for model 39 is the size 07 17 is month and year 00001 is serial number.
    
    //item code
    var itCode = parseInt(holder[0]);
    //brand
    var brand = parseInt(holder[1]+holder[2]+holder[3]);
    //model
    var model = parseInt(holder[4] + holder[5]);
    //size
    var inSize = parseInt(holder[6] + holder[7]);
    //date
    var dateIn = parseInt(holder[9] + holder[10] + holder[11] + holder[12]);
    //serial number
    var serial = parseInt(holder[13] + holder[14] + holder[15] + holder[16] + holder[17]);
    
    var inData = {
                 itCode:itCode,
                 brand:brand,
                 model:model,
                 inSize:inSize,
                 dateIn:dateIn,
                 serial:serial
                 };
    
    console.log("The data is " + itCode + " " + brand + " " + model + " " + inSize + " " + dateIn + " " + serial);
    
    return inData;    
    
}

var hi = serialSplitter(10010139071000001);
    console.log(hi);
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var config = require('./config');
var expressValidator = require('express-validator');
var mongodb= require("mongodb");
var mongoose = require('mongoose');
// master:master@ds051543.mlab.com:51543/warehouse
mongoose.connect('mongodb://localhost/manunew',function(err){
    if(err){
        console.log("Error connecting to remote database");
    }else{
        console.log("************ Successfully connected to remote database ************");
    }
});

//const MongoClient = mongodb.MongoClient;
//
//const MONGO_URL = 'mongodb://master:dbmaster1234@ds139959.mlab.com:39959/warehouse';
//
//MongoClient.connect(MONGO_URL, (err, db) => {  
//  if (err) {
//    return console.log(err);
//  }else{
//      console.log("************ Successfully connected to remote database ************");
//  }
//});

var routes = require('./routes/index');
var api = require('./routes/api');

var app = express();

var http = require('http').Server(app);

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '/public')));

app.use(expressValidator({
  errorFormatter: function(param, msg, value){
    var namespace = param.split('.'),
    root = namespace.shift(),
    formParam = root;

    while(namespace.length){
      formParam += '[' + namespace.shift() + ']';
    }
    return { 
      param: formParam,
      msg:msg,
      value:value
    };
  }
}));

var api = require('./routes/api')(app,express);
app.use('/api', api);


app.get('*',function(req,res){
  res.sendFile(__dirname + '/public/views/index.html');
});

//io.on('connection', function(socket) {
//    console.log('client connected!'); 
//});

http.listen(config.port,function(err){
  if(err){
    console.log(err);
  }else{
    console.log('listening on port 3000');
  }
})

// // catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   var err = new Error('Not Found');
//   err.status = 404;
//   next(err);
// });

// error handlers

// development error handler
// will print stacktrace
// if (app.get('env') === 'development') {
//   app.use(function(err, req, res, next) {
//     res.status(err.status || 500);
//     res.render('error', {
//       message: err.message,
//       error: err
//     });
//   });
// }

// production error handler
// no stacktraces leaked to user
// app.use(function(err, req, res, next) {
//   res.status(err.status || 500);
//   res.render('error', {
//     message: err.message,
//     error: {}
//   });
// });


module.exports = app;

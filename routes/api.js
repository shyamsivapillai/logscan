var mongoose = require('mongoose');
var express = require('express');
var Product = require('../models/product');



function serialSplitter(inSerial){
    
    var holder = inSerial.toString();
    // 10010139071700001. the first digit 1 stands for shoes. 001 stands for brand say adidas the next 01 stands for model 39 is the size 07 17 is month and year 00001 is serial number.
    
    //item code
    var itCode = parseInt(holder[0]);
    //brand
    var brand = parseInt(holder[1]+holder[2]+holder[3]);
    //model
    var model = parseInt(holder[4] + holder[5]);
    //size
    var sizeIn = parseInt(holder[6] + holder[7]);
    //date
    var dateIn = parseInt(holder[9] + holder[10] + holder[11] + holder[12]);
    //serial number
    var serial = parseInt(holder[13] + holder[14] + holder[15] + holder[16] + holder[17]);
    var inData = {
                 itCode:itCode,
                 brand:brand,
                 model:model,
                 sizeIn:sizeIn,
                 dateIn:dateIn,
                 serial:serial
                 };
    return inData;    
    
}

function serialJoin(preSerial,inSerial){
    
    var final = preSerial + inSerial;
    console.log(final);
    return parseInt(final);
    
    
}
//length of number
function getlength(number) {
  return number.toString().length;
}


function getNoShoes(inArray,serial){
    for(i=0;i<inArray.length;i++){
        if(inArray[i].serial == serial){
            return inArray[i].noShoes;
        }
    }
}

function getLocation(inArray,serial){
    for(i=0;i<inArray.length;i++){
        if(inArray[i].serial == serial){
            return inArray[i].location;
        }
    }
}

function getNoShoesLoc(inArray,loc){
    var k = 0;
    for(i=0;i<inArray.length;i++){
        if(inArray[i].location == loc){
//            return inArray[i].noShoes;
            k+=inArray[i].noShoes;
        }
    }
    return k;
}


module.exports = function(app,express){

	var api = express.Router();
    
    api.post('/createProduct',function(req,res){
        
        //itCode, date, size, noc, location
        
        var itCode = req.body.itCode;
        
        var dateIn = req.body.dateIn;
        var sizeIn = req.body.sizeIn;
        var noc = req.body.noc;
        var location = req.body.location;
        var model = req.body.model;
        var brand = req.body.brand;
        console.log(req.body);
        
        req.checkBody('itCode','Please send the Item code as well').notEmpty();
        req.checkBody('itCode','Please send the Item code as number').isInt();
        req.checkBody('dateIn','Please send the Date as well').notEmpty();
        req.checkBody('dateIn','Please send the Date as well').isInt();
        req.checkBody('sizeIn','Please send the Item SIZE as well').notEmpty();
        req.checkBody('sizeIn','Please send the Item SIZE as well').isInt();
        req.checkBody('noc',"Please send the Item's number of cartons as well").notEmpty();
        req.checkBody('noc',"Please send the Item's number of cartons as well").isInt();
        req.checkBody('location','Please send the Item\'s location as well').notEmpty();
        req.checkBody('location','Please send the Item\'s location as well').isInt();
        req.checkBody('model','Please send the Item\'s model as well').notEmpty();
        req.checkBody('model','Please send the Item\'s model as well').isInt();
        req.checkBody('brand','Please send the Item\'s brand as well').notEmpty();
        req.checkBody('brand','Please send the Item\'s brand as well').isInt();
        
        
        var errors = req.validationErrors();
        
        if(errors){
            console.log("The following errors have occurred :" + errors);
            res.json({errors:errors,msg:"Please check the input parameters and send again. Thank you", success:false});
        }else{
            
            if(location<1 || location>2){
                console.log("Invalid Number of location. You sent " + noc);
                res.json({msg:"Number of location has to be either 1 or 2. Please enter a valid value.", success:false});
            }else{
                
                Product.findOne({itCode:itCode,brand:brand,model:model,sizeIn:sizeIn},function(err,product){
                    if(product){
                        console.log("Found product-------------------------------------------");
                        console.log(product.sizeArr.length);
                        for(i=0;i<noc;i++){
                            Product.findOneAndUpdate({itCode:itCode,sizeIn:sizeIn},{$push:{"sizeArr":{dateIn:dateIn,noShoes:product.noi,location:location,serial:product.sizeArr.length+1+i}}},function(err,products){
                                
                                if(err){
                                    console.log("The following error has occurred while trying to add cartons to an existing product" + err);
                                    res.json({success:false,msg:"The following error has occurred while trying to add cartons to an existing product " + err, err:err});
                                }else{
                                    console.log("Successfully added carton number " + (i+1) + " of " + noc);
                                    
                                }
                                
                            })
                        }
                        
                        console.log("Successfully added all the products");
                        res.json({success:true,msg:"Successfully added all the products"});
                        
                        
                        
                    }else{
                    console.log("Going here *********************************");
                    var holdArr = [];
                    
                        if(!req.body.productCount){
                            
                            console.log("New product being saved, please enter the default product count in a carton of the Product");
                            res.json({success:false,msg:"When creating a new product, please send the default product count in each carton as well",err:"When creating a new product, please send the default product count in each carton as well"});
                            
                            
                        }else{
                            
                            console.log("Extra value is " + noc);
                            for(i=0;i<noc;i++){
                            console.log("Went Here " + i + "time");
                        var holder = {dateIn:dateIn,location:location,serial:i+1,noShoes:req.body.productCount};
                        
                        holdArr.push(holder);
                        
                    }
                        
                    
             var product = new Product({
                 itCode:itCode,
                 sizeArr:holdArr,
                 sizeIn:sizeIn,
                 model:model,
                 brand:brand,
                 noi:req.body.productCount
                 
             })
             
             product.save(product,function(err){
                 if(err){
                     console.log("An error occurred while saving the product " + err);
                     res.json({msg:"An error occurred while saving the product "+ err, success:false});
                 }else{
                     res.json({msg:"Successfully saved product with Item Number " + itCode + ' , date  ' + dateIn + ' , size ' + sizeIn + ' , number of cartons ' + noc + ' and location ' + location, success:true, product:product, new:true});
                 }
             })    
                            
                        }
                        
                         
                    
                    
                    }
                })
                
            }
            
        }
       
    })
    
    
    api.post('/sellItem',function(req,res){
        
//        var preCode = req.body.preCode;
//        var serial = req.body.serial;
        
        var scanCode = req.body.scanCode;
        
        console.log("Scan code recieved is " + scanCode);
        
//        req.checkBody('preCode','Please enter scan pre code').notEmpty();
//        req.checkBody('serial','Please enter serial code').notEmpty();
        
        req.checkBody('scanCode','Please enter the scan code').notEmpty();
        
        var errors = req.validationErrors();
        
        if(errors){
            
            
            console.log("The following errors occurred while trying to sell item : " + errors );
            res.json({success:false,msg:"An error occurred while trying to sell Item " + errors, err:errors});
            
        }else{
            
//            var scanCode = serialJoin(preCode,serial);
            
            var scanLength = getlength(scanCode);
            
            if(scanLength !== 17){
                console.log("Failed due to length of scanCode being " + scanLength);
                res.json({success:false,msg:"Please enter a scan code with 17 digits", err:"Scan Code length not 17 digits"});
            }else{
                
                var holder = serialSplitter(scanCode);
                
                //checking if product is sold
                
                if(holder.serial == 1){
                    
                    Product.findOne({itCode:holder.itCode,brand:holder.brand,model:holder.model,sizeIn:holder.sizeIn,"sizeArr.serial":1},function(err,product){
                    if(err){
                        console.log("Error occurred in retrieving product to be sold ---------- part 1");
                        res.json({err:err,msg:"Following error occurred while trying to retrieve product to be sold " + err, msg:"Following error occurred while trying to retrieve product to be sold"});
                    }else{
                        if(!product){
                            console.log("Product does not exist");
                            res.json({success:false,msg:"Product does not exist"});
                        }else{
                            console.log("The location of the entered item is " + getLocation(product.sizeArr,1));
                            if(getLocation(product.sizeArr,1) == 3){
                                console.log("Item already sold");
                                res.json({success:false,msg:"Item has already been sold",err:"Item has already been sold",sale:true});
                            }else{
                                
                                if(req.body.noi){
                                    
                                    if(getNoShoes(product.sizeArr,1)<req.body.noi){
                                        console.log("The input number of items exceeds the available number in the carton which is " + getNoShoes(product.sizeArr,1));
                                        res.json({success:false,msg:"The input number of items exceeds the available number in the carton which is " + getNoShoes(product.sizeArr,1),err:"The input number of items exceeds the available number in the carton which is " + getNoShoes(product.sizeArr,1)});
                                        
                                    }else{
                                        
                                        console.log("Product items available are " + getNoShoes(product.sizeArr,1) + " and requested are "  + req.body.noi)
                                       
                                        //"sizeArr.$.location":3 -- this update is not being added as the carton will be marked sold only after being emptied out completely.
                                        
                                        if(getNoShoes(product.sizeArr,1) == req.body.noi){
                                            
                                          Product.findOneAndUpdate({_id:product._id,"sizeArr.serial":1},{$set:{"sizeArr.$.location":3,"sizeArr.$.noShoes":getNoShoes(product.sizeArr,1)-req.body.noi}},function(err,product){
                                            
                                            if(err){
                        console.log("Error occurred in retrieving product to be sold =========== part 2");
                        res.json({err:err,msg:"Following error occurred while trying to retrieve product to be sold " + err, msg:"Following error occurred while trying to retrieve product to be sold"});
                    }else{
                        if(!product){
                            console.log("Product does not exist");
                            res.json({success:false,msg:"Product does not exist"});
                        }else{
                              console.log("Successfully sold product");
                              res.json({success:true,msg:"Successfully sold item"});
                        }
                    }
                                        })  
                                            
                                        }else{
                                            
                                            Product.findOneAndUpdate({_id:product._id,"sizeArr.serial":1},{$set:{"sizeArr.$.noShoes":getNoShoes(product.sizeArr,1)-req.body.noi}},function(err,product){
                                            
                                            if(err){
                        console.log("Error occurred in retrieving product to be sold =========== part 2");
                        res.json({err:err,msg:"Following error occurred while trying to retrieve product to be sold " + err, msg:"Following error occurred while trying to retrieve product to be sold"});
                    }else{
                        if(!product){
                            console.log("Product does not exist");
                            res.json({success:false,msg:"Product does not exist"});
                        }else{
                              console.log("Successfully sold product");
                              res.json({success:true,msg:"Successfully sold item"});
                        }
                    }
                                        })
                                            
                                        }
                                        
                                        
                                     
                                        
                                    }
                                    
                                    
                                }else{
                                    Product.findOneAndUpdate({itCode:holder.itCode,brand:holder.brand,model:holder.model,sizeIn:holder.sizeIn,"sizeArr.serial":1},{$set:{"sizeArr.$.location":3}},function(err,product){
                    if(err){
                        console.log("Error in selling item with Item Code " + itCode + " model " + holder.model + " size " + holder.sizeIn + " serial number " + holder.serial + " as follows : " + err );
                        res.json({success:false,msg:"Error in selling item with Item Code " + itCode + " model " + holder.model + " size " + holder.sizeIn + " serial number " + holder.serial + " as follows : " + err,err:err});
                    }else{
                        if(!product){
                            console.log("Enter valid serial number");
                            res.json({success:false,msg:"No such Item exists"});
                        }else{
                            console.log("Successfully sold item ");
                        res.json({success:true,msg:"Successfully sold Item"});
                        }
                        
                    }
                })
                                    
                                    
                                }
                                
                                
                                
                            }
                            
                            
                            
                        }
                    }
                })    
                        
                        
                        
                        
                        
                        
                    
                }else{
                    
                    
                
                Product.findOne({itCode:holder.itCode,brand:holder.brand,model:holder.model,sizeIn:holder.sizeIn,},function(err,product){
                    if(err){
                        console.log("Error occurred in retrieving product to be sold ---------- part 1");
                        res.json({err:err,msg:"Following error occurred while trying to retrieve product to be sold " + err, msg:"Following error occurred while trying to retrieve product to be sold"});
                    }else{
                        if(!product){
                            console.log("Product does not exist");
                            res.json({success:false,msg:"Product does not exist"});
                        }else{
                            console.log("The location of the entered item is " + getLocation(product.sizeArr,holder.serial));
                            if(getLocation(product.sizeArr,holder.serial) == 3){
                                console.log("Item already sold");
                                res.json({success:false,msg:"Item has already been sold",err:"Item has already been sold",sale:true});
                            }else{
                                
                                if(req.body.noi){
                                    
                                    if(getNoShoes(product.sizeArr,holder.serial)<req.body.noi){
                                        console.log("The input number of items exceeds the available number in the carton which is " + getNoShoes(product.sizeArr,holder.serial));
                                        res.json({success:false,msg:"The input number of items exceeds the available number in the carton which is " + getNoShoes(product.sizeArr,holder.serial),err:"The input number of items exceeds the available number in the carton which is " + getNoShoes(product.sizeArr,holder.serial)});
                                        
                                    }else{
                                        
                                        console.log("Product items available are " + getNoShoes(product.sizeArr,holder.serial) + " and requested are "  + req.body.noi)
                                       
                                        //"sizeArr.$.location":3 -- this update is not being added as the carton will be marked sold only after being emptied out completely.
                                        
                                        if(getNoShoes(product.sizeArr,holder.serial) == req.body.noi){
                                            
                                          Product.findOneAndUpdate({_id:product._id,"sizeArr.serial":holder.serial},{$set:{"sizeArr.$.location":3,"sizeArr.$.noShoes":getNoShoes(product.sizeArr,holder.serial)-req.body.noi}},function(err,product){
                                            
                                            if(err){
                        console.log("Error occurred in retrieving product to be sold =========== part 2");
                        res.json({err:err,msg:"Following error occurred while trying to retrieve product to be sold " + err, msg:"Following error occurred while trying to retrieve product to be sold"});
                    }else{
                        if(!product){
                            console.log("Product does not exist");
                            res.json({success:false,msg:"Product does not exist"});
                        }else{
                              console.log("Successfully sold product");
                              res.json({success:true,msg:"Successfully sold item"});
                        }
                    }
                                        })  
                                            
                                        }else{
                                            
                                            Product.findOneAndUpdate({_id:product._id,"sizeArr.serial":holder.serial},{$set:{"sizeArr.$.noShoes":getNoShoes(product.sizeArr,holder.serial)-req.body.noi}},function(err,product){
                                            
                                            if(err){
                        console.log("Error occurred in retrieving product to be sold =========== part 2");
                        res.json({err:err,msg:"Following error occurred while trying to retrieve product to be sold " + err, msg:"Following error occurred while trying to retrieve product to be sold"});
                    }else{
                        if(!product){
                            console.log("Product does not exist");
                            res.json({success:false,msg:"Product does not exist"});
                        }else{
                              console.log("Successfully sold product");
                              res.json({success:true,msg:"Successfully sold item"});
                        }
                    }
                                        })
                                            
                                        }
                                        
                                        
                                     
                                        
                                    }
                                    
                                    
                                }else{
                                    Product.findOneAndUpdate({itCode:holder.itCode,brand:holder.brand,model:holder.model,sizeIn:holder.sizeIn,"sizeArr.serial":holder.serial},{$set:{"sizeArr.$.location":3}},function(err,product){
                    if(err){
                        console.log("Error in selling item with Item Code " + itCode + " model " + holder.model + " size " + holder.sizeIn + " serial number " + holder.serial + " as follows : " + err );
                        res.json({success:false,msg:"Error in selling item with Item Code " + itCode + " model " + holder.model + " size " + holder.sizeIn + " serial number " + holder.serial + " as follows : " + err,err:err});
                    }else{
                        if(!product){
                            console.log("Enter valid serial number");
                            res.json({success:false,msg:"No such Item exists"});
                        }else{
                            console.log("Successfully sold item ");
                        res.json({success:true,msg:"Successfully sold Item"});
                        }
                        
                    }
                })
                                    
                                    
                                }
                                
                                
                                
                            }
                            
                            
                            
                        }
                    }
                })
                

                }
            }    
        }
        
    })
    
    api.post('/moveSecond',function(req,res){
        
        var scanCode = req.body.scanCode;
        
        req.checkBody('scanCode','Please enter scan code').notEmpty();
        
        var errors = req.validationErrors();
        
        if(errors){
            
            console.log("The following errors occurred while trying to move item : " + errors );
            res.json({success:false,msg:"An error occurred while trying to move Item " + errors, err:errors});
            
        }else{
            
            var scanLength = getlength(scanCode);
            
            if(scanLength !== 17){
                console.log("Failed due to length of scanCode being " + scanLength);
                res.json({success:false,msg:"Please enter a scan code with 17 digits", err:"Scan Code length not 17 digits"});
            }else{
                
                var holder = serialSplitter(scanCode);
                
                if(holder.serial == 1){
                    
                    console.log("1 debugger activated");
                     Product.findOneAndUpdate({itCode:holder.itCode,brand:holder.brand,model:holder.model,sizeIn:holder.sizeIn,"sizeArr.serial":1},{$set:{"sizeArr.$.location":2}},function(err,product){
                    if(err){
                        console.log("Error in moving item with Item Code " + itCode + " model " + holder.model + " size " + holder.sizeIn + " serial number " + holder.serial + " as follows : " + err );
                        res.json({success:false,msg:"Error in moving item with Item Code " + itCode + " model " + holder.model + " size " + holder.sizeIn + " serial number " + holder.serial + " as follows : " + err,err:err});
                    }else{
                        if(!product){
                            console.log("Enter valid serial number");
                            console.log("Error in moving item with Item Code " + holder.itCode + " model " + holder.model + " size " + holder.sizeIn + " serial number " + holder.serial + " as follows : " + err );
                            res.json({success:false,msg:"No such Item exists"});
                        }else{
                            if(getLocation(product.sizeArr,1) == 2){
                                console.log("Product is already in Ware house 2");
                                res.json({msg:"Product is already in warehouse 2",err:"Product is already in ware house 2",success:true});
                            }else{
                                if(getLocation(product.sizeArr,1)==3){
                                    console.log("Cannot reverse a sale");
                                    
                                    Product.findOneAndUpdate({itCode:holder.itCode,brand:holder.brand,model:holder.model,sizeIn:holder.sizeIn,"sizeArr.serial":1},{$set:{"sizeArr.$.location":3}},function(err,product){
                                        
                                        if(err){
                                            console.log("Error while reverting sale");
                                            
                                        }else{
                                            console.log("Successfully reverted the sale");
                                        }
                                        
                                        
                                    });
                                    
                                    res.json({success:false,msg:"Cannot move a product from location 3 to location 2"});
                                }else{
                                    console.log("Successfully moved item ");
                                res.json({success:true,msg:"Successfully moved Item"});
                                }
                                
                            }
                            
                        }
                    }
                })
                    
                    
                    
                }else{
                    
                    
                    
                
                Product.findOneAndUpdate({itCode:holder.itCode,brand:holder.brand,model:holder.model,sizeIn:holder.sizeIn,"sizeArr.serial":holder.serial},{$set:{"sizeArr.$.location":2}},function(err,product){
                    if(err){
                        console.log("Error in moving item with Item Code " + itCode + " model " + holder.model + " size " + holder.sizeIn + " serial number " + holder.serial + " as follows : " + err );
                        res.json({success:false,msg:"Error in moving item with Item Code " + itCode + " model " + holder.model + " size " + holder.sizeIn + " serial number " + holder.serial + " as follows : " + err,err:err});
                    }else{
                        if(!product){
                            console.log("Enter valid serial number");
                            console.log("Error in moving item with Item Code " + holder.itCode + " model " + holder.model + " size " + holder.sizeIn + " serial number " + holder.serial + " as follows : " + err );
                            res.json({success:false,msg:"No such Item exists"});
                        }else{
                            if(getLocation(product.sizeArr,holder.serial) == 2){
                                console.log("Product is already in Ware house 2");
                                res.json({msg:"Product is already in warehouse 2",err:"Product is already in ware house 2",success:true});
                            }else{
                                
                                if(getLocation(product.sizeArr,holder.serial)==3){
                                    console.log("Cannot reverse a sale");
                                    
                                    Product.findOneAndUpdate({itCode:holder.itCode,brand:holder.brand,model:holder.model,sizeIn:holder.sizeIn,"sizeArr.serial":holder.serial},{$set:{"sizeArr.$.location":3}},function(err,product){
                                        
                                        if(err){
                                            console.log("Error while reverting sale");
                                            
                                        }else{
                                            console.log("Successfully reverted the sale");
                                        }
                                        
                                        
                                    });
                                    
                                    res.json({success:false,msg:"Cannot move a product from location 3 to location 2"});
                                }else{
                                    console.log("Successfully moved item ");
                                res.json({success:true,msg:"Successfully moved Item"});
                                }
                                
                            }
                            
                        }
                    }
                })
                
                }
            }    
        }
        
    })


    
    api.post('/currentStock',function(req,res){
        
        
        var scanCode = req.body.scanCode;
        
        req.checkBody('scanCode','Please enter scan code').notEmpty();
        
        var errors = req.validationErrors();
        
        if(errors){
            
            console.log("The following errors occurred while trying to calculate current stock : " + errors );
            res.json({success:false,msg:"An error occurred while trying to calculate current stock " + errors, err:errors});
            
        }else{
            
            var scanLength = getlength(scanCode);
            
            if(scanLength !== 17){
                console.log("Failed due to length of scanCode being " + scanLength);
                res.json({success:false,msg:"Please enter a scan code with 17 digits", err:"Scan Code length not 17 digits"});
            }else{
                
                var holder = serialSplitter(scanCode);
                
                Product.findOne({itCode:holder.itCode,model:holder.model,brand:holder.brand,sizeIn:holder.sizeIn},function(err,product){
                    if(err){
                        console.log("Error in calculating current stock for item with Item Code " + itCode + " model " + holder.model + " size " + holder.sizeIn + " serial number " + holder.serial + " as follows : " + err );
                        res.json({success:false,msg:"Error in calculating current stock for item with Item Code " + itCode + " model " + holder.model + " size " + holder.sizeIn + " serial number " + holder.serial + " as follows : " + err,err:err});
                    }else{
                        
                        var stockOne = 0;
                        var stockTwo = 0;
                        var sold = 0;
                        
                        
                        for(i=0;i<product.sizeArr.length;i++){
                            if(product.sizeArr[i].location == 1){
                                stockOne +=1;
                            }else if(product.sizeArr[i].location == 2){
                                stockTwo +=1;
                            }else if(product.sizeArr[i].location == 3){
                                sold +=1;
                            }
                        }
                        
                        
                        var stock = {stockOne: stockOne,stockTwo: stockTwo, sold: sold, total: product.sizeArr.length};
                        
                        console.log("Successfully calculated stock available");
                        res.json({success:true,msg:"Successfully calculated stock available",stock:stock});
                    }
                })
                
                
                
            }
        }
       
    })

    
    api.get('/allProducts',function(req,res){
        
        Product.find({},function(err,products){

            if(err){
                console.log("Error retrieving products");
                res.json({success:false,err:err,msg:"Error occurred while retrieving the products: " + err});
            }else{

                if(products){
                    
                    console.log("Successfully recieved the products");
                    res.json({success:true,products:products});
                    
                }else{
                    console.log("No products recieved/entered in database");
                    res.json({success:false,msg:"No products recieved/entered in database",err:"No products recieved / entered in database"});
                }
            
            }
        
        })
        
    })
    
    api.post('/checkItem',function(req,res){
        
        var scanCode = req.body.scanCode;
        
        req.checkBody('scanCode','Please enter the scan Code as well').notEmpty();
        
        var errors = req.validationErrors();
        
        if(errors){
            console.log("Error occurred in checking item at level 1");
            res.json({success:false,err:errors,msg:"Error occurred in checking item at level 1"});
        }else{
            
            var scanLength = getlength(scanCode);
            
            if(scanLength !== 12){
                console.log("Failed due to length of scanCode being " + scanLength);
                res.json({success:false,msg:"Please enter a scan code with 12 digits", err:"Scan Code length not 17 digits"});
            }else{
                
                var holder = serialSplitter(scanCode);
                
                Product.findOne({itCode:holder.itCode,model:holder.model,brand:holder.brand,sizeIn:holder.sizeIn},function(err,product){
                    if(err){
                        console.log("Error in checking current stock for item with Item Code " + itCode + " model " + holder.model + " size " + holder.sizeIn + " serial number " + holder.serial + " as follows : " + err );
                        res.json({success:false,msg:"Error in checking current stock for item with Item Code " + itCode + " model " + holder.model + " size " + holder.sizeIn + " serial number " + holder.serial + " as follows : " + err,err:err});
                    }else{
                        
                        if(!product){
                            console.log("No product with the given scan code found, please check again");
                            res.json({success:false,msg:"No product with the given scan code found, please check again",err:"No product with the given scan code found, please check again"});
                        }else{
                            
                            
                        var returnStockOne = getNoShoesLoc(product.sizeArr,1);
                        
                        var returnStockTwo = getNoShoesLoc(product.sizeArr,2);
                        
                        console.log("Successfully calculated stock available");
                        res.json({success:true,msg:"Successfully calculated stock available",returnStockOne:returnStockOne,returnStockTwo:returnStockTwo});
                            
                        }
                        
                    }
                })
                
                
                
            }
            
        }
        
    })
    
    
api.route('/')

		.post(function(req,res){
			var story = new Story({
				creator: req.decoded.id,
				content: req.body.content
			});
			story.save(function(err,newStory){
				if(err){
					res.send(err);
					return
				}
				io.emit('story',newStory)
				res.json({message:"New Story created!"});
			});
		})

		.get(function(req,res){
			Story.find({creator:req.decoded.id},function(err,stories){
				if(err){
					res.send(err);
					return
				}
				res.json(stories);
			})
		})


		api.get('/me',function(req,res){
			res.json(req.decoded);
		})

	return api

}
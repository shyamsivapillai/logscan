var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ProductSchema = new Schema({
    
    itCode : {type : Number, required: true},
    model: {type : Number},
    sizeIn : {type:Number},
    brand:{type:Number},
    sizeArr: [{dateIn:{type:Number},location:{type:Number},serial:{type:Number}, noShoes:{type:Number}}],
    noi:{type:Number}

});

module.exports = mongoose.model('Product',ProductSchema);



